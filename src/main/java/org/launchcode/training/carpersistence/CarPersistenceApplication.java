package org.launchcode.training.carpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarPersistenceApplication.class, args);
	}
}
